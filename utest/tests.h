#pragma once

#include <cstdlib>
#include <stdexcept>
#include <string>
#include <sstream>
#include <doctest/doctest.h>
//#include "random.h"
#include "string/string.h"

//random<float>  SRand;


TEST_CASE("String find")
{
    using string = std::string;
    CHECK_EQ(0, owl::find(string(""), string("")));
    CHECK_EQ(0, owl::find(string(""), string("abcdefghijklmnopqrstuvwxyz")));
    CHECK_EQ(0, owl::find(string("a"), string("a")));
    CHECK_EQ(0, owl::find(string("a"), string("aaa")));
    CHECK_EQ(0, owl::find(string("aa"), string("aaa")));
    CHECK_EQ(0, owl::find(string("aaa"), string("aaa")));
    
    CHECK_EQ(string::npos, owl::find(string("aaaa"), string("aaa")));
    CHECK_EQ(string::npos, owl::find(string("aaaaaa"), string("aa")));
    CHECK_EQ(string::npos, owl::find(string("aaaaaa"), string("a")));
    
    CHECK_EQ(1, owl::find(string("bab"), string("ababa")));
    CHECK_EQ(4, owl::find(string("o"), string("hello, world")));
    CHECK_EQ(2, owl::find(string("ll"), string("hello, world")));
    
    CHECK_EQ(string::npos, owl::find(string("goodbye"), string("hello, world")));
    CHECK_EQ(string::npos, owl::find(string("no"), string("hello, world")));

    CHECK_EQ(31, owl::find(string(")"), string("int main(int argc, char* argv[])")));
    CHECK_EQ(0, owl::find(string("i"), string("int main(int argc, char* argv[])")));
    CHECK_EQ(0, owl::find(string("int"), string("int main(int argc, char* argv[])")));
    CHECK_EQ(9, owl::find(string("int a"), string("int main(int argc, char* argv[])")));
}

TEST_CASE("String find huge string")
{
    std::srand(std::time(nullptr));

    int const str_len = 5000;
    int const pat_max_len = 500;

    std::string str(str_len, 'a');
    for (int i = 0; i < str_len; ++i)
    {
        str[i] += static_cast<char>(std::rand() % 26);
    }

    for (int i = 1; i < pat_max_len; ++i)
    {
        std::string pat(i, 'a');
        for (int j = 0; j < i; ++j)
        {
            pat[j] += static_cast<char>(std::rand() % 26);
        }
        CHECK_EQ(str.find(pat), owl::find(pat, str));
    }
}


TEST_CASE("String reverse")
{
    std::string s1("");
    owl::reverse(&s1);
    CHECK_EQ(std::string(""), s1);

    std::string s2("a");
    owl::reverse(&s2);
    CHECK_EQ(std::string("a"), s2);

    std::string s3("ab");
    owl::reverse(&s3);
    CHECK_EQ(std::string("ba"), s3);

    std::string s4("abc");
    owl::reverse(&s4);
    CHECK_EQ(std::string("cba"), s4);
}

TEST_CASE("String reverse wchar")
{
    std::wstring s1(L"");
    owl::reverse(&s1);
    CHECK_EQ(std::wstring(L""), s1);

    std::wstring s2(L"a");
    owl::reverse(&s2);
    CHECK_EQ(std::wstring(L"a"), s2);

    std::wstring s3(L"ab");
    owl::reverse(&s3);
    CHECK_EQ(std::wstring(L"ba"), s3);

    std::wstring s4(L"abc");
    owl::reverse(&s4);
    CHECK_EQ(std::wstring(L"cba"), s4);
}

TEST_CASE("String ends_with")
{
    CHECK_EQ(true, owl::ends_with(std::string_view("hello"), std::string_view("")));
    CHECK_EQ(true, owl::ends_with(std::string_view("hello"), std::string_view("o")));
    CHECK_EQ(true, owl::ends_with(std::string_view("hello"), std::string_view("lo")));
    CHECK_EQ(false, owl::ends_with(std::string_view("hello"), std::string_view("ll")));
    CHECK_EQ(false, owl::ends_with(std::string_view("hello"), std::string_view("h")));
    CHECK_EQ(false, owl::ends_with(std::string_view("hello"), std::string_view("he")));
    CHECK_EQ(true, owl::ends_with(std::string_view("hello"), std::string_view("hello")));
}

TEST_CASE("String maximal suffix")
{
    {
        std::wstring s1(L"A");
        auto ms = owl::help::max_suffix(s1, owl::help::compare_char_asc<wchar_t>);
        std::wstring::size_type x = 0, y = 1;
        CHECK_EQ(ms.first, x);
        CHECK_EQ(ms.second, y);
    }
    {
        std::wstring s1(L"AA");
        auto ms = owl::help::max_suffix(s1, owl::help::compare_char_asc<wchar_t>);
        std::wstring::size_type x = 0, y = 1;
        CHECK_EQ(ms.first, x);
        CHECK_EQ(ms.second, y);
    }
    {
        std::wstring s1(L"ABA");
        auto ms = owl::help::max_suffix(s1, owl::help::compare_char_asc<wchar_t>);
        std::wstring::size_type x = 1, y = 2;
        CHECK_EQ(ms.first, x);
        CHECK_EQ(ms.second, y);
    }
    {
        std::wstring s1(L"ABAB");
        auto ms = owl::help::max_suffix(s1, owl::help::compare_char_asc<wchar_t>);
        std::wstring::size_type x = 1, y = 2;
        CHECK_EQ(ms.first, x);
        CHECK_EQ(ms.second, y);
    }
    {
        std::wstring s1(L"ABABA");
        auto ms = owl::help::max_suffix(s1, owl::help::compare_char_asc<wchar_t>);
        std::wstring::size_type x = 1, y = 2;
        CHECK_EQ(ms.first, x);
        CHECK_EQ(ms.second, y);
    }
    {
        std::wstring s1(L"ABAABA");
        auto ms = owl::help::max_suffix(s1, owl::help::compare_char_asc<wchar_t>);
        std::wstring::size_type x = 1, y = 3;
        CHECK_EQ(ms.first, x);
        CHECK_EQ(ms.second, y);
    }
    {
        std::wstring s1(L"ABCDEF");
        auto ms = owl::help::max_suffix(s1, owl::help::compare_char_asc<wchar_t>);
        std::wstring::size_type x = 5, y = 1;
        CHECK_EQ(ms.first, x);
        CHECK_EQ(ms.second, y);
    }
}

TEST_CASE("String match")
{
    {
        std::string s1("");
        std::string s2("");
        CHECK_EQ(0, owl::match(s2, s1));
    }
    {
        std::string s1("DGJAGIODHKLDDG");
        std::string s2("");
        CHECK_EQ(0, owl::match(s2, s1));
    }
    {
        std::string s1("DGJAGIODHKLDDG");
        std::string s2("RHSFGDFG");
        CHECK_EQ(std::string::npos, owl::match(s2, s1));
    }
    {
        std::string s1("ABCDEF");
        std::string s2("ABCDEF");
        CHECK_EQ(0, owl::match(s2, s1));
    }
    {
        std::string s1("ABCDEF");
        std::string s2("AA");
        CHECK_EQ(std::string::npos, owl::match(s2, s1));
    }
    {
        std::string s1("ABCDEF");
        std::string s2("AAB");
        CHECK_EQ(std::string::npos, owl::match(s2, s1));
    }
    {
        std::string s1("ABCDEF");
        std::string s2("ABA");
        CHECK_EQ(std::string::npos, owl::match(s2, s1));
    }
    {
        std::string s1("ABCDEF");
        std::string s2("A");
        CHECK_EQ(0, owl::match(s2, s1));
    }
    {
        std::string s1("ABCDEF");
        std::string s2("B");
        CHECK_EQ(1, owl::match(s2, s1));
    }
    {
        std::string s1("ABCDEF");
        std::string s2("C");
        CHECK_EQ(2, owl::match(s2, s1));
    }
    {
        std::string s1("ABCDEF");
        std::string s2("D");
        CHECK_EQ(3, owl::match(s2, s1));
    }
    {
        std::string s1("ABCDEF");
        std::string s2("E");
        CHECK_EQ(4, owl::match(s2, s1));
    }
    {
        std::string s1("ABCDEF");
        std::string s2("F");
        CHECK_EQ(5, owl::match(s2, s1));
    }
    {
        std::string s1("DBAABAACAA");
        std::string s2("ABAABAA");
        CHECK_EQ(std::string::npos, owl::match(s2, s1));
    }
    {
        std::string s1("DBAABAACAA");
        std::string s2("BAABAA");
        CHECK_EQ(1, owl::match(s2, s1));
    }
    {
        std::string s1("DBAABAACAA");
        std::string s2("BAACA");
        CHECK_EQ(4, owl::match(s2, s1));
    }
    {
        std::string s1("DBAABAACAA");
        std::string s2("CAA");
        CHECK_EQ(7, owl::match(s2, s1));
    }
}



/*
struct SkipListFixture : public ::testing::Test
{
    SkipListFixture()
        : mSList1(SRand)
        , mSList2(SRand)
    {
    }

    void fillRandomly(size_t count, int mod = std::numeric_limits<int>::max())
    {
        for (size_t i = 0; i < count; ++i)
        {
            int num = mRand() % mod;
            mSList1.insert(num);
            mSet1.insert(num);
            num = mRand();
            mSList2.insert(num);
            mSet2.insert(num);
        }
    }

    void fillRandomlyRange(size_t count)
    {
        std::list<int> listInt;
        for (size_t i = 0; i < count; ++i)
        {
            int const num = mRand();
            listInt.push_back(num);
        }
        mSList1.insert(std::cbegin(listInt), std::cend(listInt));
        mSet1.insert(std::cbegin(listInt), std::cend(listInt));
    }

    void clear()
    {
        mSList1.clear();
        mSList2.clear();
        mSet1.clear();
        mSet2.clear();
    }

    random<int>  mRand;
    skip_list<int, random<float>>  mSList1;
    skip_list<int, random<float>>  mSList2;
    std::set<int>  mSet1;
    std::set<int>  mSet2;
};

struct RandomSkipListFixture : public ::testing::Test
{
    RandomSkipListFixture()
        : mSList(SRand)
    {
        for (size_t i = 0; i < 1000; ++i)
        {
            int const num = mRand();
            mSList.insert(num);
            mSet.insert(num);
        }
    }

    random<int>  mRand;
    skip_list<int, random<float>>  mSList;
    std::set<int> mSet;
};

owl::skip_list<int, random<float>> createSListInt(float prob)
{
    return owl::skip_list<int, random<float>>(SRand, prob);
}

void compareSize(std::set<int> const& st, owl::skip_list<int, random<float>> const& sl)
{
    EXPECT_EQ(st.size(), sl.size());
}

void compare(std::set<int> const& st, owl::skip_list<int, random<float>> const& sl)
{
    auto sIt = std::cbegin(st);
    auto slIt = std::cbegin(sl);
    size_t counter = 0;
    bool result = true;
    for (; sIt != std::cend(st) && slIt != std::cend(sl);)
    {
        EXPECT_EQ(*sIt, *slIt);
        ++sIt;
        ++slIt;
        ++counter;
    }
    EXPECT_EQ(st.size(), counter);
}

TEST(SkipListTest, WrongArgument)
{
    EXPECT_THROW(createSListInt(-0.00001f), std::invalid_argument);
    EXPECT_THROW(createSListInt(1.00001f), std::invalid_argument);
}

TEST_F(RandomSkipListFixture, IterateOver)
{
    {
        ASSERT_EQ(mSet.size(), mSList.size());
        skip_list<int, random<float>>::iterator slIt = std::begin(mSList);
        auto sIt = std::begin(mSet);
        size_t counter = 0;
        for (; slIt != std::end(mSList) && sIt != std::end(mSet);)
        {
            EXPECT_EQ(*sIt, *slIt);
            ++sIt;
            ++slIt;
            ++counter;
        }
        ASSERT_EQ(mSet.size(), counter);
    }
    {
        ASSERT_EQ(mSet.size(), mSList.size());
        skip_list<int, random<float>>::const_iterator slIt = std::begin(mSList);
        auto sIt = std::begin(mSet);
        size_t counter = 0;
        for (; slIt != std::end(mSList) && sIt != std::end(mSet);)
        {
            EXPECT_EQ(*sIt, *slIt);
            ++sIt;
            ++slIt;
            ++counter;
        }
        ASSERT_EQ(mSet.size(), counter);
    }
    {
        ASSERT_EQ(mSet.size(), mSList.size());
        auto slIt = std::cbegin(mSList);
        auto sIt = std::cbegin(mSet);
        size_t counter = 0;
        for (; slIt != std::cend(mSList) && sIt != std::cend(mSet);)
        {
            EXPECT_EQ(*sIt, *slIt);
            ++sIt;
            ++slIt;
            ++counter;
        }
        ASSERT_EQ(mSet.size(), counter);
    }
}

TEST_F(SkipListFixture, Swap)
{
    fillRandomly(100);
    compareSize(mSet1, mSList1);
    compareSize(mSet2, mSList2);
    compare(mSet1, mSList1);
    compare(mSet2, mSList2);

    std::swap(mSList1, mSList2);
    compareSize(mSet1, mSList2);
    compareSize(mSet2, mSList1);
    compare(mSet1, mSList2);
    compare(mSet2, mSList1);
}

TEST_F(SkipListFixture, ClearEmpty)
{
    for (int i = 0; i < 100; ++i)
    {
        EXPECT_TRUE(mSList1.empty());
        EXPECT_TRUE(mSList2.empty());
        fillRandomly(100);
        EXPECT_FALSE(mSList1.empty());
        EXPECT_FALSE(mSList2.empty());
        mSList1.clear();
        mSList2.clear();
    }
    EXPECT_TRUE(mSList1.empty());
    EXPECT_TRUE(mSList2.empty());
}

TEST_F(SkipListFixture, FillRange)
{
    fillRandomlyRange(100);
    compareSize(mSet1, mSList1);
    compare(mSet1, mSList1);

    std::swap(mSList1, mSList2);
    compareSize(mSet1, mSList2);
    compareSize(mSet2, mSList1);
    compare(mSet1, mSList2);
    compare(mSet2, mSList1);

    mSList1.clear();
    mSet1.clear();
    auto initList = { 1, 2, 3, 4, 5, 12, -1 };
    mSList1.insert(initList);
    mSet1.insert(initList);
    compareSize(mSet1, mSList1);
    compare(mSet1, mSList1);
}

TEST_F(SkipListFixture, Erase)
{
    for (int i = 0; i < 100; ++i)
    {
        fillRandomly(100);
        {
            skip_list<int, random<float>>::iterator slIt = std::begin(mSList1);
            while (!mSList1.empty())
            {
                slIt = mSList1.erase(slIt);
            }
            EXPECT_TRUE(mSList1.empty());
            EXPECT_EQ(0, mSList1.size());
        }
        clear();
    }
}

TEST_F(SkipListFixture, EraseRandomly)
{
    for (int i = 0; i < 100; ++i)
    {
        fillRandomly(500);

        std::vector<int> vect;
        std::copy(std::cbegin(mSList1), std::cend(mSList1), std::back_inserter(vect));

        while (!vect.empty())
        {
            int const idx = mRand() % vect.size();
            mSList1.erase(vect[idx]);
            vect.erase((std::cbegin(vect) + idx));
            EXPECT_EQ(vect.size(), mSList1.size());
        }
    }
}

TEST_F(SkipListFixture, CountIsZeroOrOne)
{
    for (int i = 0; i < 100; ++i)
    {
        fillRandomly(500);

        int const num = mRand();
        int const count = mSList1.count(num);
        bool const contains = mSList1.contains(num);
        EXPECT_TRUE(count == 0 || count == 1);
        EXPECT_TRUE((contains && count == 1) || (!contains && count == 0));
    }
}

TEST_F(SkipListFixture, Find)
{
    for (int i = 1; i < 100; ++i)
    {
        for (int j = 0; j < i; ++j)
        {
            int num = mRand();
            while (mSet1.find(num) != std::cend(mSet1))
                num = mRand();
            mSList1.insert(num);
            mSet1.insert(num);
        }

        for (auto num : mSet1)
        {
            skip_list<int, random<float>>::iterator slIt = mSList1.find(num);
            skip_list<int, random<float>>::const_iterator slcIt = mSList1.find(num);
            EXPECT_NE(slIt, std::end(mSList1));
            EXPECT_NE(slcIt, std::cend(mSList1));
        }
    }
}

TEST_F(SkipListFixture, EqualRange)
{
    fillRandomly(1000, 10000);
    int forExisting = 0;
    int forNonExist = 0;

    int i = 0;
    while (forExisting < 10 || forNonExist < 10)
    {
        auto eqRange = mSList1.equal_range(i);
        if (eqRange.first != std::end(mSList1))
            EXPECT_GE(*(eqRange.first), i);

        bool contains = mSList1.contains(i);
        if (contains)
        {
            EXPECT_EQ(i, *(eqRange.first));
            ++forExisting;
        }
        else
        {
            EXPECT_EQ(eqRange.first, eqRange.second);
            ++forNonExist;
        }
        ++i;
    }
}

TEST_F(SkipListFixture, LowerBound)
{
    fillRandomly(1000, 10000);
    for (int i = 0; i < 10000; ++i)
    {
        int const num = mRand() % 10000;
        auto lwS = mSet1.lower_bound(num);
        skip_list<int, random<float>>::iterator lwL = mSList1.lower_bound(num);
        if (lwS == std::end(mSet1))
            EXPECT_EQ(std::end(mSList1), lwL);
        else
            EXPECT_EQ(*lwS, *lwL);

        auto upS = mSet1.lower_bound(num);
        skip_list<int, random<float>>::iterator upL = mSList1.lower_bound(num);
        if (upS == std::end(mSet1))
            EXPECT_EQ(std::end(mSList1), upL);
        else
            EXPECT_EQ(*upS, *upL);
    }

    clear();
    fillRandomly(1000, 10000);
    for (int i = 0; i < 10000; ++i)
    {
        int const num = mRand() % 10000;
        auto lwS = mSet1.lower_bound(num);
        owl::skip_list<int, random<float>>::const_iterator lwL = mSList1.lower_bound(num);
        if (lwS == std::end(mSet1))
            EXPECT_EQ(std::end(mSList1), lwL);
        else
            EXPECT_EQ(*lwS, *lwL);

        auto upS = mSet1.lower_bound(num);
        owl::skip_list<int, random<float>>::const_iterator upL = mSList1.lower_bound(num);
        if (upS == std::end(mSet1))
            EXPECT_EQ(std::end(mSList1), upL);
        else
            EXPECT_EQ(*upS, *upL);
    }
}

TEST_F(SkipListFixture, UpperBound)
{
    fillRandomly(1000, 10000);
    for (int i = 0; i < 10000; ++i)
    {
        int const num = mRand() % 10000;
        auto lwS = mSet1.upper_bound(num);
        skip_list<int, random<float>>::iterator lwL = mSList1.upper_bound(num);
        if (lwS == std::end(mSet1))
            EXPECT_EQ(std::end(mSList1), lwL);
        else
            EXPECT_EQ(*lwS, *lwL);

        auto upS = mSet1.upper_bound(num);
        skip_list<int, random<float>>::iterator upL = mSList1.upper_bound(num);
        if (upS == std::end(mSet1))
            EXPECT_EQ(std::end(mSList1), upL);
        else
            EXPECT_EQ(*upS, *upL);
    }

    clear();
    fillRandomly(1000, 10000);
    for (int i = 0; i < 10000; ++i)
    {
        int const num = mRand() % 10000;
        auto lwS = mSet1.upper_bound(num);
        owl::skip_list<int, random<float>>::const_iterator lwL = mSList1.upper_bound(num);
        if (lwS == std::end(mSet1))
            EXPECT_EQ(std::end(mSList1), lwL);
        else
            EXPECT_EQ(*lwS, *lwL);

        auto upS = mSet1.upper_bound(num);
        owl::skip_list<int, random<float>>::const_iterator upL = mSList1.upper_bound(num);
        if (upS == std::end(mSet1))
            EXPECT_EQ(std::end(mSList1), upL);
        else
            EXPECT_EQ(*upS, *upL);
    }
}
*/
