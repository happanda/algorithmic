#pragma once

#include <chrono>
#include <random>

#include "doctest.h"
#include "math/vec.h"
#include "perlin.h"
#include "perlin2d.h"


int const sRepeatRandoms = 128;
owl::math::index2<int> sWorkingPerlinSize{ 2, 64 };

TEST_CASE("Perlin 1D Random Gradient")
{
    std::mt19937 gen(std::chrono::high_resolution_clock::now().time_since_epoch().count());

    SUBCASE("Input size < 2")
    {
        std::uniform_int_distribution<> unidist(std::numeric_limits<int>::min(), -1);

        for (int repeats = 0; repeats < sRepeatRandoms; ++repeats)
        {
            int const k = unidist(gen);
            REQUIRE(k < 2);
            owl::rnd::perlin_rand_gradient<float, int> prd{ k };
            CHECK(prd.size() == 2);
        }

        owl::rnd::perlin_rand_gradient<float, int> prd0{ 0 };
        CHECK(prd0.size() == 2);
        owl::rnd::perlin_rand_gradient<float, int> prd1{ 1 };
        CHECK(prd1.size() == 2);
        owl::rnd::perlin_rand_gradient<float, int> prd2{ 2 };
        CHECK(prd2.size() == 2);
    }

    SUBCASE("Input size >= 2")
    {
        std::uniform_int_distribution<> unidist(sWorkingPerlinSize.x, sWorkingPerlinSize.y);

        for (int repeats = 0; repeats < sRepeatRandoms; ++repeats)
        {
            int const k = unidist(gen);
            REQUIRE(k >= 2);
            owl::rnd::perlin_rand_gradient<float, int> prd{ k };
            CHECK(prd.size() == k);
        }
    }

    SUBCASE("Check return values in [-1, 1]")
    {
        std::uniform_int_distribution<> unidist(sWorkingPerlinSize.x, sWorkingPerlinSize.y);

        for (int repeats = 0; repeats < sRepeatRandoms; ++repeats)
        {
            int const k = unidist(gen);
            owl::rnd::perlin_rand_gradient<float, int> prd{ k };
            for (int i = 0; i < prd.size(); ++i)
            {
                CHECK_MESSAGE(prd(i) >= -1, " where i = ", i);
                CHECK_MESSAGE(prd(i) <= 1, " where i = ", i);
            }
        }
    }
}

TEST_CASE("Perlin 1D")
{
    std::mt19937 gen(std::chrono::high_resolution_clock::now().time_since_epoch().count());

    SUBCASE("Input size < 2")
    {
        owl::rnd::perlin prd0{ 0 };
        CHECK(prd0.size() == 2);
        owl::rnd::perlin prd1{ 1 };
        CHECK(prd1.size() == 2);
        owl::rnd::perlin prd2{ 2 };
        CHECK(prd2.size() == 2);
    }

    SUBCASE("Check return values in [-1, 1]")
    {
        std::uniform_int_distribution<> unidist(sWorkingPerlinSize.x, sWorkingPerlinSize.y);
        std::uniform_real_distribution<float> unidistreal(0, 1);

        for (int repeats = 0; repeats < sRepeatRandoms; ++repeats)
        {
            auto size = (std::uint32_t)unidist(gen);
            owl::rnd::perlin prd{ size };
            if (size < 2)
            {
                CHECK(prd.size() == 2);
            }
            else
            {
                CHECK(prd.size() == size);
            }

            for (int points = 0; points < sRepeatRandoms; ++points)
            {
                float point = unidistreal(gen);
                float value = prd(point);
                CHECK_MESSAGE(value >= -1, " where size = ", prd.size(), ", point = ", point);
                CHECK_MESSAGE(value <= 1, " where size = ", prd.size(), ", point = ", point);
                CHECK_MESSAGE(value == prd(point), " where size = ", prd.size(), ", point = ", point);
            }
        }
    }

    SUBCASE("First derivative check")
    {
        float const xdiff = 1e-5;
        float const epsilon = 1;

        auto derivative = [&xdiff](auto func, float x)
        {
            float left = x - xdiff;
            float right = x + xdiff;
            REQUIRE(left >= 0);
            REQUIRE(left <= 1);
            REQUIRE(right >= 0);
            REQUIRE(right <= 1);
            float lvalue = func(left);
            float rvalue = func(right);
            float deriv = (rvalue - lvalue) / (xdiff * 2);
            return deriv;
        };

        std::uniform_int_distribution<> unidist(sWorkingPerlinSize.x, sWorkingPerlinSize.y);

        for (int repeats = 0; repeats < 8; ++repeats)
        {
            auto size = (std::uint32_t)unidist(gen);
            owl::rnd::perlin prd{ size };

            float point = xdiff * 2;
            float prevDeriv = derivative(prd, point);

            while (point <= 1 - xdiff * 2)
            {
                auto deriv = derivative(prd, point);

                CHECK_MESSAGE(std::abs(prevDeriv - deriv) < epsilon, " point = ", point);

                prevDeriv = deriv;
                point += xdiff;
            }
        }
    }

}

TEST_CASE("Perlin 2D Random Gradient")
{
    std::mt19937 gen(std::chrono::high_resolution_clock::now().time_since_epoch().count());

    SUBCASE("Input size < 2")
    {
        std::uniform_int_distribution<> uniintdistNeg(std::numeric_limits<int>::min(), -1);
        std::uniform_int_distribution<> uniintdistPos(sWorkingPerlinSize.x, sWorkingPerlinSize.y);

        for (int repeats = 0; repeats < sRepeatRandoms; ++repeats)
        {
            int const s1 = uniintdistNeg(gen);
            int const s2 = uniintdistPos(gen);
            REQUIRE(s1 < 2);
            REQUIRE(s2 >= 2);

            int sizes[14][2] = { {s1, s2}, {s2, s1}, {0, s1}, {s1, 0}, {1, s1}, {s1, 1}, {2, s1}, {s1, 2},
                {0, s2}, {s2, 0}, {1, s2}, {s2, 1}, {2, s2}, {s2, 2} };

            for (auto size : sizes)
            {
                owl::rnd::perlin_rand_gradient2d<float, int> prd{ size[0], size[1] };
                int prdSizes[] = { std::max(2, size[0]), std::max(2, size[1]) };
                CHECK(prd.size() == owl::math::index2<int>{ prdSizes[0], prdSizes[1] });
            }
        }
    }

    SUBCASE("Input size >= 2")
    {
        std::uniform_int_distribution<> unidistX(sWorkingPerlinSize.x, sWorkingPerlinSize.y);
        std::uniform_int_distribution<> unidistY(sWorkingPerlinSize.x, sWorkingPerlinSize.y);

        for (int repeats = 0; repeats < sRepeatRandoms; ++repeats)
        {
            int const sx = unidistX(gen);
            int const sy = unidistY(gen);
            REQUIRE(sx >= 2);
            REQUIRE(sy >= 2);
            owl::rnd::perlin_rand_gradient2d<float, int> prd{ sx, sy };
            CHECK(prd.size() == owl::math::index2<int>{ sx, sy });
        }
    }

    SUBCASE("Check return values in [-1, 1]")
    {
        std::uniform_int_distribution<> unidistX(sWorkingPerlinSize.x, sWorkingPerlinSize.y);
        std::uniform_int_distribution<> unidistY(sWorkingPerlinSize.x, sWorkingPerlinSize.y);

        for (int repeats = 0; repeats < sRepeatRandoms; ++repeats)
        {
            int const sx = unidistX(gen);
            int const sy = unidistY(gen);
            REQUIRE(sx >= 2);
            REQUIRE(sy >= 2);
            owl::rnd::perlin_rand_gradient2d<float, int> prd{ sx, sy };
            for (int i = 0; i < prd.size().x; ++i)
            {
                for (int j = 0; j < prd.size().y; ++j)
                {
                    CHECK_MESSAGE(prd(i, j).x >= -1, " where i = ", i, ", j = ", j);
                    CHECK_MESSAGE(prd(i, j).y >= -1, " where i = ", i, ", j = ", j);
                    CHECK_MESSAGE(prd(i, j).x <= 1, " where i = ", i, ", j = ", j);
                    CHECK_MESSAGE(prd(i, j).y <= 1, " where i = ", i, ", j = ", j);
                }
            }
        }
    }
}

TEST_CASE("Perlin 2D")
{
    std::mt19937 gen(std::chrono::high_resolution_clock::now().time_since_epoch().count());

    SUBCASE("Input size < 2")
    {
        owl::rnd::perlin prd0{ 0 };
        CHECK(prd0.size() == 2);
        owl::rnd::perlin prd1{ 1 };
        CHECK(prd1.size() == 2);
        owl::rnd::perlin prd2{ 2 };
        CHECK(prd2.size() == 2);
    }

    SUBCASE("Check return values in [-1, 1]")
    {
        std::uniform_int_distribution<> unidist(sWorkingPerlinSize.x, sWorkingPerlinSize.y);
        std::uniform_real_distribution<float> unidistreal(0, 1);

        for (int repeats = 0; repeats < sRepeatRandoms; ++repeats)
        {
            auto size = (std::uint32_t)unidist(gen);
            owl::rnd::perlin prd{ size };
            if (size < 2)
            {
                CHECK(prd.size() == 2);
            }
            else
            {
                CHECK(prd.size() == size);
            }

            for (int points = 0; points < sRepeatRandoms; ++points)
            {
                float point = unidistreal(gen);
                float value = prd(point);
                CHECK_MESSAGE(value >= -1, " where size = ", prd.size(), ", point = ", point);
                CHECK_MESSAGE(value <= 1, " where size = ", prd.size(), ", point = ", point);
                CHECK_MESSAGE(value == prd(point), " where size = ", prd.size(), ", point = ", point);
            }
        }
    }

    SUBCASE("First derivative check")
    {
        using vec2 = owl::math::vec2<float>;

        float const diff = 1e-5;
        float const epsilon = 1;

        auto derivative = [&diff](auto func, vec2 const& at)
        {
            auto left = at - diff;
            auto right = at + diff;
            REQUIRE(left.x >= 0);
            REQUIRE(left.y >= 0);
            REQUIRE(left.x <= 1);
            REQUIRE(left.y <= 1);
            REQUIRE(right.x >= 0);
            REQUIRE(right.y >= 0);
            REQUIRE(right.x <= 1);
            REQUIRE(right.y <= 1);

            //std::cout << func(at.x, at.y) << ": (" << at.x << ", " << at.y << ")" << std::endl;
            //std::cout << func(left.x, at.y) << ": (" << at.x << ", " << at.y << ")" << std::endl;
            //std::cout << func(right.x, at.y) << ": (" << at.x << ", " << at.y << ")" << std::endl;
            //std::cout << func(at.x, left.y) << ": (" << at.x << ", " << at.y << ")" << std::endl;
            //std::cout << func(at.x, right.y) << ": (" << at.x << ", " << at.y << ")" << std::endl;
            float lvalueX = func(left.x, at.y);
            float rvalueX = func(right.x, at.y);
            float lvalueY = func(at.x, left.y);
            float rvalueY = func(at.x, right.y);
            vec2 shift = vec2{ rvalueX, rvalueY } - vec2{ lvalueX, rvalueX };
            auto deriv = shift / (diff * 2);
            return deriv;
        };

        std::uniform_int_distribution<> unidist(sWorkingPerlinSize.x, sWorkingPerlinSize.y);

        for (int repeats = 0; repeats < 4; ++repeats)
        {
            auto size = (std::uint32_t)unidist(gen);
            owl::rnd::perlin2d<float> prd{ size, size };

            vec2 point{ diff * 2, diff * 2 };

            point.y = diff * 2;
            while (point.y <= 1 - diff * 2)
            {
                point.x = diff * 2;
                auto prevDeriv = derivative(prd, point);

                while (point.x <= 1 - diff * 2)
                {
                    auto deriv = derivative(prd, point);

                    CHECK_MESSAGE(std::abs(prevDeriv - deriv).x < epsilon, " point = ", point.x);
                    CHECK_MESSAGE(std::abs(prevDeriv - deriv).y < epsilon, " point = ", point.y);

                    prevDeriv = deriv;
                    point.x += 32 * diff;
                }
                point.y += 32 * diff;
            }
        }
    }

}
