#pragma once

namespace owl
{
	template <class RealNumber>
	constexpr RealNumber smoothstep(RealNumber t)
	{
		if (t > 0 && t < 1)
			return 3 * t * t - 2 * t * t * t;
		else if (t <= 0)
			return 0;
		return 1;
	}

	template <class RealNumber>
	constexpr RealNumber smootherstep(RealNumber t)
	{
		if (t > 0 && t < 1)
			return 6 * t * t * t * t * t - 15 * t * t * t * t + 10 * t * t * t;
		else if (t <= 0)
			return 0;
		return 1;
	}
} // namespace owl
