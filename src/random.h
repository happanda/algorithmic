#pragma once

#include <chrono>
#include <random>

namespace owl
{
namespace rnd
{
    template <class RealNumber>
    struct random
    {
        random(RealNumber lower = 0, RealNumber upper = 1,
            std::mt19937::result_type seed = std::chrono::high_resolution_clock::now().time_since_epoch().count())
            : mRandGen(seed)
            , mUniDist(lower, upper)
        {
        }

        RealNumber operator()()
        {
            return mUniDist(mRandGen);
        }

        void seed(std::mt19937::result_type val)
        {
            mRandGen.seed(val);
        }

    private:
        std::mt19937  mRandGen;
        std::uniform_real_distribution<RealNumber>  mUniDist;
    };

    template <>
    struct random<int>
    {
        random(int lower = 0, int upper = std::numeric_limits<int>::max(),
            std::mt19937::result_type seed = std::chrono::high_resolution_clock::now().time_since_epoch().count())
            : mRandGen(static_cast<std::mt19937::result_type>(
                seed))
            , mUniDist(lower, upper)
        {
        }

        int operator()()
        {
            return mUniDist(mRandGen);
        }

        void seed(std::mt19937::result_type val)
        {
            mRandGen.seed(val);
        }

    private:
        std::mt19937  mRandGen;
        std::uniform_int_distribution<int>  mUniDist;
    };
}// namespace rnd
} // namespace owl
