#pragma once

#include <cstdint>
#include <vector>
#include "math/vec.h"
#include "math/ind.h"
#include "random.h"
#include "step_functions.h"

namespace owl::rnd
{
	template<class RealNumber, class Size>
	class perlin_rand_gradient2d;

	template <class RealNumber = float,
		class GradientFunc = perlin_rand_gradient2d<RealNumber, std::uint32_t>>
		class perlin2d
	{
	public:

		using real_t = RealNumber;
		using size_t = std::uint32_t;
		using vec2 = owl::math::vec2<real_t>;
		using ind2 = owl::math::index2<size_t>;
		using SmoothStepFunc = real_t(*)(real_t x);

	private:

		GradientFunc mGradient;
		SmoothStepFunc mSmoothStep;
		vec2 mInverseInterval;

	public:

		perlin2d(ind2 gradients = { 32, 32 },
				SmoothStepFunc smoothstepfunc = smootherstep<real_t>)
			: mGradient(std::max({ gradients.x, size_t{2} }),
						std::max({ gradients.y, size_t{2} }))
			, mSmoothStep(smoothstepfunc)
			, mInverseInterval({ 1, 1 })
		{
			mInverseInterval.x /= (real_t)(mGradient.size() - 1).x;
			mInverseInterval.y /= (real_t)(mGradient.size() - 1).y;
		}

		perlin2d(size_t sizeX, size_t sizeY,
				SmoothStepFunc smoothstepfunc = smootherstep<real_t>)
			: perlin2d(ind2{ sizeX, sizeY }, smoothstepfunc)
		{
		}

		template<typename TestClass = GradientFunc>
		perlin2d(GradientFunc gradientFunc,
				SmoothStepFunc smoothstepfunc = smootherstep<real_t>,
				typename std::enable_if<!std::numeric_limits<GradientFunc>::is_integer>::type* = 0)
			: mGradient(gradientFunc)
			, mSmoothStep(smoothstepfunc)
			, mInverseInterval({ 1, 1 })
		{
			mInverseInterval /= (mGradient.size() - 1);
		}

		real_t operator()(real_t atX, real_t atY) const
		{
			return operator()(vec2{ atX, atY });
		}

		real_t operator()(vec2 at) const
		{
			auto size = (mGradient.size() - 1);
			vec2 sizer{ (real_t)size.x, (real_t)size.y };
			vec2 atScaled = at * sizer;
			vec2 atScaledFloor = std::floor(atScaled);

			ind2 interval = { (size_t)(atScaledFloor.x), (size_t)(atScaledFloor.y) };

			vec2 residue = (atScaled - atScaledFloor);

			vec2 leftOffset = -residue;
			vec2 rightOffset = leftOffset + (real_t)1;


			real_t lowerLeft = math::dot(leftOffset, mGradient(interval.x, interval.y));
			real_t upperLeft = (interval.y < size.y) ?
				math::dot(vec2{ leftOffset.x, rightOffset.y }, mGradient(interval.x, interval.y + 1))
				: 0;
			real_t lowerRight = (interval.x < size.x) ?
				math::dot(vec2{ rightOffset.x, leftOffset.y }, mGradient(interval.x + 1, interval.y))
				: 0;
			real_t upperRight = (interval.x < size.x && interval.y < size.y) ?
				math::dot(rightOffset, mGradient(interval.x + 1, interval.y + 1))
				: 0;

			real_t lower = lowerLeft + mSmoothStep(residue.x) * (lowerRight - lowerLeft);
			real_t upper = upperLeft + mSmoothStep(residue.x) * (upperRight - upperLeft);
			real_t result = lower + mSmoothStep(residue.y) * (upper - lower);
			return result;
		}

		GradientFunc& gradient()
		{
			return mGradient;
		}

		size_t size() const
		{
			return mGradient.size();
		}
	};


	template<class RealNumber, class Size>
	class perlin_rand_gradient2d
	{
	public:

		using vec2 = owl::math::vec2<RealNumber>;

	private:

		random<RealNumber> mRand{ 0, 1, 3 };
		random<int> mRandI{ 0, 3 };
		std::vector<std::vector<vec2>> mGradients;
		std::vector<std::vector<vec2>> mGradientsCopy;

	public:

		perlin_rand_gradient2d(Size sizeX, Size sizeY)
			: mGradients(std::max({ sizeX, Size{ 2 } }))
			, mGradientsCopy(std::max({ sizeX, Size{ 2 } }))
		{
			sizeY = std::max({ sizeY, Size{ 2 } });
			for (std::vector<vec2>& row : mGradients)
			{
				row.resize(sizeY);
			}
			for (std::vector<vec2>& row : mGradientsCopy)
			{
				row.resize(sizeY);
				for (vec2& vec : row)
				{
					vec2 predefinedGrads[] = {
						vec2{1, 1},
						vec2{1, -1},
						vec2{-1, 1},
						vec2{-1, -1},
					};
					vec = predefinedGrads[mRandI()];
					//RealNumber randAngle = mRand() * 3.141592653589793 * 2;
					//vec.x = std::cos(randAngle);
					//vec.y = std::sin(randAngle);
				}
			}
		}

		void updateGradient(Size x, Size y, RealNumber max)
		{
			RealNumber seed = x + y;
			random<RealNumber> rand{ 0, max, (unsigned)seed };
			random<int> randi{ 0, 3, (unsigned)seed };
			RealNumber randAngle = rand();
			int randInd = randi();
			vec2 predefinedGrads[] = {
						vec2{1, 1},
						vec2{1, -1},
						vec2{-1, 1},
						vec2{-1, -1},
			};
			auto rot1 = vec2{ cos(max), -sin(max) };
			auto rot2 = vec2{ sin(max), cos(max) };
			mGradients[x][y] = vec2{ math::dot(mGradientsCopy[x][y], rot1),
				math::dot(mGradientsCopy[x][y], rot2) };
		}

		vec2 operator()(Size x, Size y) const
		{
			return mGradients[x][y];
		}

		math::index2<Size> size() const
		{
			return { (Size)mGradients.size(), (Size)mGradients.front().size() };
		}
	};
} // namespace owl
